package com.company;


import java.util.ArrayList;

public abstract class FormaGeometrica {


    private ArrayList<Integer> lados = new ArrayList<Integer>();

    public ArrayList<Integer> getLados() {
        return lados;
    }

    public void setLados(ArrayList<Integer> lados) {
        this.lados = lados;
    }



    public abstract double calculoArea();
    public abstract void definirLados();


}
