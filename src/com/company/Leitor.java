package com.company;

import java.util.Scanner;

public class Leitor {


     public void processar() {
         boolean blnSairLoop = true;
         while (blnSairLoop) {
             System.out.println("Informar os lados da Forma :");
             Scanner entrada = new Scanner(System.in);
             avaliarForma(entrada.nextInt());
             System.out.println("Sair do Programa (S/N) ?");
             Scanner saida = new Scanner(System.in);
             blnSairLoop =  saida.next().toUpperCase().equals("S")?false:true;
         }
     }


     public void avaliarForma(int lado) {
         switch (lado) {
             case 1:
                 System.out.println("Isso é um Circulo");
                 Circulo circulo = new Circulo();
                 System.out.println("Area:" + circulo.calculoArea());
                 break;
             case 2:
                 System.out.println("Isso é um Retangulo");
                 Retangulo ret = new Retangulo();
                 System.out.println("Area:" + ret.calculoArea());
                 break;
             case 3:
                 System.out.println("Isso é um Triangulo");
                 Triangulo triangulo = new Triangulo();
                 System.out.println("Area:" + triangulo.calculoArea());
                 break;
             default:
                 System.out.println("Forma não definida");
         }
     }
}
