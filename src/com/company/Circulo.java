package com.company;

import java.util.Scanner;

public class Circulo extends FormaGeometrica {

    public Circulo() {
        this.definirLados();
    }

    public double getRaio() {
        return raio;
    }

    public void setRaio(double raio) {
        this.raio = raio;
    }

    private double raio ;

    @Override
    public double calculoArea() {
        return Math.pow(this.raio , 2) * Math.PI;
    }

    @Override
    public void definirLados() {
        Scanner entrada = new Scanner(System.in);
        System.out.print("informar Raio :");
        this.setRaio(entrada.nextDouble());
    }
}
