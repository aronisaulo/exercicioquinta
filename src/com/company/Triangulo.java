package com.company;

import java.util.Scanner;

public class Triangulo extends FormaGeometrica{
    private  double[] lados = new double[3];

    public Triangulo() {
        this.definirLados();
    }
    public boolean validarTriangulo() {
        if ( ((lados[0] + lados[1]) > lados[2] ) &&
                ((lados[0] + lados[2]) > lados[1] ) &&
                ((lados[1] + lados[2]) > lados[0] )
            ) {

            return true;
        } else return false;
    }

    @Override
    public double calculoArea() {
        if (!this.validarTriangulo()) {
            System.out.println(" Triangulo inválido");
            return 0;
        } else {
        double area = (lados[0] + lados[1] + lados[2]) / 2 ;
        return Math.sqrt( area * (area - lados[0]) * (area - lados[1]) * (area - lados[2]) );
        }

    }

    @Override
    public void definirLados() {

        for (int l=0 ; l<3 ; l++){
            Scanner entrada = new Scanner(System.in);
            System.out.print("Informar Lado"+ (l+1) +" :");
            lados[l] =  entrada.nextDouble();
        }

    }
}
