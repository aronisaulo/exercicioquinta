package com.company;

import java.util.Scanner;

public class Retangulo extends FormaGeometrica {

    private double altura , comprimento ;

    public Retangulo() {
        this.definirLados();
    }

    public double getAltura() {
        return altura;
    }

    public void setAltura(double altura) {
        this.altura = altura;
    }

    public double getComprimento() {
        return comprimento;
    }

    public void setComprimento(double comprimento) {
        this.comprimento = comprimento;
    }

    @Override
    public double calculoArea() {

        return this.altura * this.comprimento;
    }

    @Override
    public void definirLados() {
        Scanner entrada = new Scanner(System.in);
        System.out.print("informar Altura :");
        this.setAltura(entrada.nextDouble());
        System.out.print("infomrar Comprimento :");
        this.setComprimento(entrada.nextDouble());

    }


}
